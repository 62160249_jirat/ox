/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.oxworld;

/**
 *
 * @author ACER
 */
import java.util.Scanner;

public class OXBoard {

    static int round = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            System.out.println(row + " " + col);
            if (row < 3 && col < 3 && row >= 0 && col >= 0) {

                if (table[row][col] == '-') {
                    table[row][col] = player;
                    round++;
                    break;
                }
                
                System.out.println("Error: table at row and col is not emply!!!");
            } else {
                System.out.println("Row and Col must be 1-3.Please try again!");
            }
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkBackslash() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static int i;

    static void checkSlash() {
        for (int row = 0, i = 2; row < 3; row++, i--) {
            if (table[row][i] != player) {
                return;
            }

        }
        isFinish = true;
        winner = player;
    }

    static void checkDraw() {
        if (round == 9 && winner == '-') {
            isFinish = true;
        }

    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkBackslash();
        checkSlash();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }

    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!!");
        }

    }

    static void showBye() {
        System.out.println("Bye bye....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();
    }
}
